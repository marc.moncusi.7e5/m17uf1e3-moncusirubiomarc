# M17UF1E3

Crear o editar components per afegir característiques:

## Recopilació de dades

Crear un component que recopila dades del personatge: Nom, Tipus de personatge, alçada, velocitat, distància a recórrer. Aquest component ha de ser consultat per altres classes i editable des de l'inspector.

## Animació

Crear una animació amb els sprites aportats. L'animació s'ha de fer a partir del Update (no animator).

Bonus: Ha de tenir una velocitat de 12 Fps.

## Moviment autònom

Crear un component que dona moviment autònom al personatge. Camina la distància en unitats , dona mitja volta i tornar a "patrullar" en direcció contrària.

Bonus: A l'acabar un recorregut, el personatge ha de parar, inclosa animació, un temps determinat. I després continua el seu patrullatge.

Bonus: Crear un component que substitueix el de moviment per un altre que dona el moviment del personatge a l'usuari.

## Associar velocitat i pes

El personatge anirà més lent caminant com més gran sigui la variable "weight" del component "DataPlayer". La velocitat del personatge és inversament proporcional al pes del personatge.

## Habilitat gegantina

El nostre personatge tindrà l'habilitat de transformar-se en un gegant durant un temps limitat.
La mida és directament proporcional a l'alçada "height" del component creat per recollir dades.
Per activar l'habilitat és necessari fer clic en un botó. El personatge no creixerà de forma immediata sinó a mesura que camini fins a arribar a N vegades la seva alçada. Aquesta habilitat, un cop activada, ha de tenir un temps de refredament abans de tornar a ser activada.

## HUD

Crear un HUD amb el número de frames que reprodueix el joc, una icona que representi el personatge i un text que recull el nom de player.

Bonus: Crear els elements d'UI necessaris perquè l'usuari pugui modificar la velocitat de moviment, la distància a recórrer, o el temps de duració de l'habilitat gegantina.

Bonus: crear un text amb el Nom i tipus de personatge que acompanya al personatge en tot moment.

## Nova Escena

Crear una escena a part de presentació amb una imatge, text (títol provisional) i un input text que reculli el nom del nostre personatge. El nom de afegit ha de mostrar-se al HUD. Afegir un botó "Start" que porti a l'escena on s'ha realitzat la resta de punts anteriors. Aquest nom es guardarà a la variable "Nom" de la classe "DataPlayer" anteriorment esmentada.
