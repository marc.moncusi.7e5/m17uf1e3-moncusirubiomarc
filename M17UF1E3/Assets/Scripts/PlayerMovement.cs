using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    //Up,
    //Down,
    Left,
    Right
}

public class PlayerMovement : MonoBehaviour
{
    private PlayerData _playerData;
    private Direction _direction;

    // Start is called before the first frame update
    void Start()
    {
        _playerData = gameObject.GetComponent<PlayerData>();
        _direction = Direction.Left;
    }

    // Update is called once per frame
    void Update()
    {
            if (Input.GetAxis("Horizontal") != 0) _playerData.MovementAnimation();
        if (Input.GetAxis("Horizontal") > 0)
        {
            _direction = Direction.Right;
            _playerData.SetLookingDirection(true);
        }else if (Input.GetAxis("Horizontal") < 0)
        {
            _direction = Direction.Left;
            _playerData.SetLookingDirection(false);
        }
            transform.position += new Vector3(Input.GetAxis("Horizontal") * _playerData.GetSpeed() * Time.deltaTime, Input.GetAxis("Vertical") * _playerData.GetSpeed() * Time.deltaTime, 0);
    }
}
