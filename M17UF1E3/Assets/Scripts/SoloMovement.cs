using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloMovement : MonoBehaviour
{
    private PlayerData _playerData;
    private float _initialPosition;
    private Direction _direction;
    private float _leftLimit;
    private float _rightLimit;

    private float _waitingTime;
    private float _timer;

    // Start is called before the first frame update
    void Start()
    {
        _playerData = gameObject.GetComponent<PlayerData>();
        _initialPosition = transform.position.x;
        _rightLimit = _initialPosition + (_playerData.GetDistance() / 2);
        _leftLimit = _initialPosition - (_playerData.GetDistance() / 2);
        _direction = Direction.Left;
        _waitingTime = _timer = 3;
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        if (_timer > _waitingTime) {
            switch (_direction)
            {
                case Direction.Left:
                    _playerData.SetLookingDirection(false);
                    transform.position -= new Vector3(_playerData.GetSpeed() * Time.deltaTime, 0, 0);
                    _playerData.MovementAnimation();
                    if (transform.position.x < _leftLimit)
                    {
                        _direction = Direction.Right;
                        _timer = 0;
                    }
                    break;
                case Direction.Right:
                    _playerData.SetLookingDirection(true);
                    transform.position += new Vector3(_playerData.GetSpeed() * Time.deltaTime, 0, 0);
                    _playerData.MovementAnimation();
                    if (transform.position.x > _rightLimit)
                    {
                        _direction = Direction.Left;
                        _timer = 0;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    public void RestartParameters()
    {
        _initialPosition = transform.position.x;
        _rightLimit = _initialPosition + (_playerData.GetDistance() / 2);
        _leftLimit = _initialPosition - (_playerData.GetDistance() / 2);
    }
}
