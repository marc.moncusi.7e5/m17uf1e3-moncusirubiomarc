using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _textField;
    [SerializeField] private GameObject _fpsField;
    [SerializeField] private GameObject _movementButton;

    private int frameCount = 0;
    private double dt = 0.0;
    private double updateRate = 0.5; //1 Update every 2 seconds

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("PlayerCharacter");
        _textField = GameObject.Find("Player_Name");
        _fpsField = GameObject.Find("FPS");
        _movementButton = GameObject.Find("Switch Button");

        _textField.GetComponent<Text>().text = _player.GetComponent<PlayerData>().GetName();
        _fpsField.GetComponent<Text>().text = (1 / Time.deltaTime).ToString() + "FPS";
        _movementButton.gameObject.GetComponent<Button>().onClick.AddListener(() => _player.GetComponent<PlayerData>().SwitchMovement());
    }

    // Update is called once per frame
    void Update()
    {
        frameCount++;
        dt += Time.deltaTime;
        if (dt > 1.0 / updateRate)
        {
            _fpsField.GetComponent<Text>().text = (frameCount /dt).ToString("F2") + "FPS";
            frameCount = 0;
            dt -= 1.0 / updateRate;
            
        }

    }
}
