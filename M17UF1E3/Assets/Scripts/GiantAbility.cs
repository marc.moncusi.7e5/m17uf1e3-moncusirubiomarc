using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantAbility : MonoBehaviour
{
    private PlayerData _playerData;
    private PlayerSize _size;
    private float _coolDown = 5;
    private float _abilityTime = 10;
    private float _timer;


    // Start is called before the first frame update
    void Start()
    {
        _playerData = gameObject.GetComponent<PlayerData>();
        _size = PlayerSize.Normal;
        _timer = _coolDown;
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        switch (_size)
        {
            case PlayerSize.Normal:
                if(_timer >= _coolDown && Input.GetKeyDown(KeyCode.Space))
                {
                    _timer = 0;
                    _size = PlayerSize.Giant;
                }

                if (_playerData.GetNormalSize() <= transform.localScale.x)
                {
                    transform.localScale -= new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime, 0);
                }
                break;
            case PlayerSize.Giant:
                if (_timer > _abilityTime)
                {
                    _timer = 0;
                    _size = PlayerSize.Normal;
                }
                if (_playerData.GetNormalSize() * 2 >= transform.localScale.x)
                {
                    transform.localScale += new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime, 0);
                }
                break;
            default:
                break;
        }
    }
}
