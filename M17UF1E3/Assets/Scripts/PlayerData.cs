using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerSize
{
    Normal,
    Giant
}

public enum CharacterType
{
    Barbarian,
    Bard,
    Cleric,
    Druid,
    Hunter,
    Jester,
    Monk,
    Rogue,
    SpellWeaver,    
    Warlock,
    Warrior,
    Wizard
}


public class PlayerData : MonoBehaviour
{
    [SerializeField] private string _playerName;
    [SerializeField] private CharacterType _characterType;
    [SerializeField] private float _height;
    [SerializeField] private float _weight;
    private float _defaultSpeed = 100;
    private PlayerMovement _pMovement;
    private SoloMovement _sMovement;
    [SerializeField] private float _movementDistance;
    [SerializeField] private Sprite[] _sprite;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private float _frameRate = 12;
    private float _timer = 0;
    private int _spriteArrayLength;
    private int _indexSprite = 0;
    //private Controller _controller;

    private void Awake()
    {
        _playerName = Controller.playerName;
        _movementDistance = 10f;
        _height = transform.localScale.x;
    }

    // Start is called before the first frame update
    void Start()
    {
        _weight = 60f;
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _pMovement = gameObject.GetComponent<PlayerMovement>();
        _pMovement.enabled = !_pMovement.enabled;
        _sMovement = gameObject.GetComponent<SoloMovement>();
        _spriteArrayLength = _sprite.Length;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MovementAnimation()
    {
        _timer += Time.deltaTime;
        if (_timer >= _spriteArrayLength / _frameRate)
        {
            _spriteRenderer.sprite = _sprite[_indexSprite];
            _indexSprite = (_indexSprite < _spriteArrayLength - 1) ? (_indexSprite + 1) : 0;
            _timer = 0;
        }
    }

    public string GetName()
    {
        return _playerName;
    }

    public float GetSpeed()
    {
        return _defaultSpeed / _weight;
    }

    public float GetDistance()
    {
        return _movementDistance;
    }

    //False to look to the left, True to look to the right
    public void SetLookingDirection(bool flip)
    {
        _spriteRenderer.flipX = flip;
    }

    public float GetNormalSize()
    {
        return _height;
    }

    public void SwitchMovement()
    {
        _pMovement.enabled = !_pMovement.enabled;
        _sMovement.enabled = !_sMovement.enabled;
        if (_sMovement.enabled)
        {
            _sMovement.RestartParameters();
        }

    }
}
