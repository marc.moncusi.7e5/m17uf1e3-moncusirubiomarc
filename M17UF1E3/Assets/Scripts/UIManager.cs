using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject _inputText;
    // Start is called before the first frame update
    void Start()
    {
        _inputText = GameObject.Find("Name_Field");
        this.gameObject.GetComponent<Button>().onClick.AddListener(() => CheckName());
    }
    void StartGame(string name)
    {
        Controller.playerName = name;
        SceneManager.LoadScene("SampleScene");
    }
    void CheckName()
    {
        string name = _inputText.GetComponent<InputField>().text;
        if (name != "")
        {
            StartGame(name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
